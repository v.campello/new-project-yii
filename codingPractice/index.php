<?

function getCount($str) {
    $arr = ['a','e','i','o','u'];
    $vowelsCount = 0;
    foreach (str_split($str) as $givenChar) {
        foreach($arr as $vowel){
            if($vowel == $givenChar){
                $vowelsCount += 1;
            }
        }
    }
    echo $vowelsCount . PHP_EOL;
    return $vowelsCount;
}
getCount('  teste');


/** FIRST PRACTICES */

function multiply($a, $b)
{
    return $a * $b;
}


function even_or_odd(int $n): string
{
    if ($n % 2 == 0) {
        return "Even";
    } else {
        return "Odd";
    }
}


function opposite($n)
{
    return $n * (-1) ;
}


function positive_sum($arr)
{
    $result = 0;
    foreach ($arr as $number) {
        if ($number >= 0) {
            $result += $number;
        }
    }
    return $result;
}
?>
