<?php

namespace app\controllers;

use app\models\Cliente;
use Yii;
use yii\web\Controller;

class InsertController extends Controller
{
    public function actionIndex()
    {
        $clientes = [
            ['nome' => 'Kilderson Sena'],
            ['nome' => 'Cálcio'],
            ['nome' => 'Railton'],
            ['nome' => 'Carlos Galvão'],
            ['nome' => 'Bruno'],
            ['nome' => 'Eric'],
        ];

        Yii::$app->db->createCommand()->batchInsert(Cliente::tableName(), ['nome'], $clientes)->execute();

        /*foreach ($clientes as $cliente) {
            $row = new Cliente();
            $row->nome = $cliente['nome'];
            $row->save();
        }*/

        echo 'Utilizando batchinsert com sucesso';
    }
}
