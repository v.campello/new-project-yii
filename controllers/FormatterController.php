<?php

namespace app\controllers;

use Yii;
use yii\base\Controller;

class FormatterController extends Controller
{
    public function actionIndex()
    {
        $appLang = Yii::$app->language;

        /** @var MyFormatter $formatter */
        $formatter = Yii::$app->formatter;

        echo "<h2>{$appLang}</h2>";

        echo "
        <p>CNPJ: {$formatter->asCnpj('11444777000122')}</p>
        <p>CEP: {$formatter->asCep('30730500')}</p>
        <p>CPF: {$formatter->asCpf('11122233344')}</p>
        <p>Datas Formato PHP: {$formatter->asDate("1992-02-02")}</p>
        <p>Datas Formato: {$formatter->asDate("2016-03-20")}</p>
        <p>Datas: {$formatter->asDate("2016-03-20", "full")}</p>
        <p>NText: {$formatter->asNText("vinicius\nalves\ncampello")}</p>
        <p>E-mail's: {$formatter->asEmail('vinicius.alves.leme@hotmail.com')}</p>
        <p>Percentuais: {$formatter->asPercent(0.123, 2)}</p>
        <p>Booleans: {$formatter->asBoolean(true)}</p>
        ";
    }
}
