<?php

// use yii\bootstrap4\ActiveForm;
// use yii\helpers\Html;

?>

<h2>Formulário de Cadastro - Yii2</h2>
<hr>

<?php $form = \yii\bootstrap4\ActiveForm::begin() ?>

    <?= $form->field($model, 'nome') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'idade') ?>

    <div>
        <?= yii\helpers\Html::submitButton('Enviar Dados', ['class' => 'btn btn-primary']) ?>
    </div>

<?php \yii\bootstrap4\ActiveForm::end(); ?>
