<?php

namespace app\modules\financeiro;

use Yii;

class FinanceiroModule extends yii\base\Module
{
    public $layout = 'blank';
    public function init()
    {

        parent::init();
        Yii::configure($this, require(__DIR__ . '/config/main.php'));
    }
}
