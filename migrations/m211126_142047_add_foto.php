<?php

use app\models\Cliente;
use yii\db\Migration;

/**
 * Class m211126_142047_add_foto
 */
class m211126_142047_add_foto extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn(\app\models\Cliente::tableName(), 'foto', $this->string(60));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn(Cliente::tableName(), 'foto');
    }
}
