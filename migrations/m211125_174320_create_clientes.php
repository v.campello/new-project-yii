<?php

use yii\db\Migration;

/**
 * Class m211125_174320_create_clientes
 */
class m211125_174320_create_clientes extends Migration
{
    /**
     * {@inheritdoc}
     */
    // public function safeUp()
    // {
    // }



    //  public function safeDown()
    //  {
    //      echo "m211125_174320_create_clientes cannot be reverted.\n";

    //      return false;
    //     }



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('{{%clientes}}', [
            'id' => $this->primaryKey(),
            'nome' => $this->string(60)->notNull()
        ]);
    }

    public function down()
    {
        // echo "m211125_174320_create_clientes cannot be reverted.\n";
        $this->dropTable('{{%clientes}}');
    }
}
